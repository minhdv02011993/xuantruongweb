<?php

namespace App\Http\Controllers\webs;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class ExperienceController extends Controller
{
    public function index(Request $request)
    {
        $category = Category::where('slug', 'kinh-nghiem')->first();
        $posts = Post::where('category_id', $category->id)->orderByDesc('id')->get();
        $recent_posts = Post::where('category_id', $category->id)->orderByDesc('id')->limit(5)->get();
        return view('layout.experience')->with([
            'posts' => $posts,
            'recent_posts' => $recent_posts,
        ]);
    }

    public function detail(Request $request, $post_slug)
    {
        $category = Category::where('slug', 'kinh-nghiem')->first();
        $recent_posts = Post::where('category_id', $category->id)->orderByDesc('id')->limit(5)->get();
        $post = Post::where('category_id', $category->id)
            ->where('slug', $post_slug)
            ->first();
        return view('layout.experience_detail')->with([
            'post' => $post,
            'recent_posts' => $recent_posts,
        ]);
    }
}
