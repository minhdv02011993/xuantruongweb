<?php

namespace App\Http\Controllers\webs;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class IntroduceController extends Controller
{
    public function index(Request $request)
    {
        $category = Category::where('slug', 'gioi-thieu')->first();
        $post = Post::where('category_id', $category->id)
            ->orderByDesc('id')
            ->first();
        $categoryService = Category::where('slug', 'dich-vu')->first();
        $categories = Category::where('category_id', $categoryService->id)->get();
        $categoryIds = [$categoryService->id];
        foreach ($categories as $category) {
            array_push($categoryIds, $category->id);
        }
        $recent_posts = Post::whereIn('category_id', $categoryIds)->orderByDesc('id')->limit(5)->get();
        return view('layout.introduce')->with([
            'post' => $post,
            'recent_posts' => $recent_posts,
        ]);
    }
}
