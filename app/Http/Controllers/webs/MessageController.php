<?php

namespace App\Http\Controllers\webs;

use App\Http\Controllers\Controller;
use App\Mail\Message as MailMessage;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    public function store(Request $request) {
        $message = Message::create($request->all());
        
        Mail::to(config('mail.mail_to_address'))->send(new MailMessage($message));
        return response()->json($message, 200);
    }
}
