<?php

namespace App\Http\Controllers\webs;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class NewController extends Controller
{
    public function index(Request $request)
    {
        $category = Category::where('slug', 'tin-tuc')->first();
        $categories = Category::where('category_id', $category->id)->get();
        $categoryIds = [$category->id];
        foreach ($categories as $category) {
            array_push($categoryIds, $category->id);
        }
        $posts = Post::whereIn('category_id', $categoryIds)->orderByDesc('id')->get();
        $recent_posts = Post::whereIn('category_id', $categoryIds)->orderByDesc('id')->limit(5)->get();
        return view('layout.new')->with([
            'posts' => $posts,
            'categories' => $categories,
            'recent_posts' => $recent_posts,
        ]);
    }

    public function detail(Request $request, $post_slug)
    {
        $category = Category::where('slug', 'tin-tuc')->first();
        $checkIsCategorySlug = Category::where('slug', $post_slug)->first();
        $categories = Category::where('category_id', $category->id)->get();
        $categoryIds = [$category->id];
        foreach ($categories as $category) {
            array_push($categoryIds, $category->id);
        }
        $posts = Post::whereIn('category_id', $categoryIds)
            ->when($checkIsCategorySlug, function($q) use($checkIsCategorySlug) {
                $q->where('category_id', $checkIsCategorySlug->id);
            })
            ->orderByDesc('id')
            ->get();
        $recent_posts = Post::whereIn('category_id', $categoryIds)->orderByDesc('id')->limit(5)->get();
        if ($checkIsCategorySlug) {
           
            return view('layout.new')->with([
                'posts' => $posts,
                'categories' => $categories,
                'recent_posts' => $recent_posts,
            ]);
        }

        $post = Post::where('slug', $post_slug)
            ->first();
        return view('layout.new_detail')->with([
            'post' => $post,
            'categories' => $categories,
            'recent_posts' => $recent_posts
        ]);
    }

    public function detail2(Request $request, $category_slug, $post_slug)
    {
        $category = Category::where('slug', 'tin-tuc')->first();
        $checkIsCategorySlug = Category::where('slug', $category_slug)->first();
        $categories = Category::where('category_id', $category->id)->get();
        $categoryIds = [$category->id];
        foreach ($categories as $category) {
            array_push($categoryIds, $category->id);
        }
        $posts = Post::whereIn('category_id', $categoryIds)
            ->when($category_slug, function($q) use($checkIsCategorySlug) {
                $q->where('category_id', $checkIsCategorySlug->id);
            })
            ->orderByDesc('id')
            ->get();
        $recent_posts = Post::whereIn('category_id', $categoryIds)->orderByDesc('id')->limit(5)->get();
        if ($checkIsCategorySlug) {
           
            return view('layout.new')->with([
                'posts' => $posts,
                'categories' => $categories,
                'recent_posts' => $recent_posts,
            ]);
        }

        $post = Post::where('slug', $post_slug)
            ->first();
        return view('layout.new_detail')->with([
            'post' => $post,
            'categories' => $categories,
            'recent_posts' => $recent_posts
        ]);
    }
}
