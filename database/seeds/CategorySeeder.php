<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'category_id' => 0,
            'name' => 'tin tức',
            'slug' => 'tin-tuc',
        ]);

        Category::create([
            'category_id' => 0,
            'name' => 'kinh nghiệm',
            'slug' => 'kinh-nghiem'
        ]);

        Category::create([
            'category_id' => 0,
            'name' => 'dịch vụ',
            'slug' => 'dich-vu'
        ]);

        Category::create([
            'category_id' => 1,
            'name' => 'tin tổng hợp',
            'slug' => 'tin-tong-hop'
        ]);

        Category::create([
            'category_id' => 1,
            'name' => 'góc giải trí',
            'slug' => 'goc-giai-tri'
        ]);

        
        Category::create([
            'category_id' => 3,
            'name' => 'dịch vụ vận chuyển',
            'slug' => 'dich-vu-van-chuyen'
        ]);
        Category::create([
            'category_id' => 3,
            'name' => 'dịch vụ chuyển tiền',
            'slug' => 'dich-vu-chuyen-tien'
        ]);
        Category::create([
            'category_id' => 3,
            'name' => 'order hàng trung quốc',
            'slug' => 'order-hang-trung-quoc'
        ]);
        Category::create([
            'category_id' => 3,
            'name' => 'tìm nguồn hàng',
            'slug' => 'tim-nguon-hang'
        ]);
        Category::create([
            'category_id' => 3,
            'name' => 'tìm nhà máy sản xuất',
            'slug' => 'tim-nha-may-san-xuat'
        ]);
    }
}
