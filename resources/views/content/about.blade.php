<section id="section-content" class="content page-12 moto-section" data-widget="section" data-container="section">
    <div class="moto-widget moto-widget-row row-fixed moto-bg-color5_4 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-justify-content_center" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sasa" style="" data-bg-position="left top">


        <div class="container-fluid">
            <div class="row" data-container="container">


                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="-" data-animation="">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <h1 class="moto-text_system_9">ABOUT</h1>
                        </div>
                    </div>
                </div>
                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                    <div id="wid_1587313943_9rmk4ba9k" class="moto-widget moto-widget-breadcrumbs moto-align-right moto-align-left_mobile-h moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto   moto-preset-default" data-widget="breadcrumbs" data-preset="default">
                        <div class="moto-widget-breadcrumbs__wrapper">
                            <ul class="moto-widget-breadcrumbs__list">
                                <li class="moto-widget-breadcrumbs__item">
                                    <a href="/" class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_link"><span class="moto-widget-breadcrumbs__item-text">Home</span></a>
                                </li>
                                <li class="moto-widget-breadcrumbs__item">
                                    <span class="moto-widget-breadcrumbs__item-delimiter">/</span>
                                    <span class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_current"><span class="moto-widget-breadcrumbs__item-text">About</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <div class="moto-widget moto-widget-row row-fixed moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="lama" style="" data-bg-position="left top" data-draggable-disabled="">


        <div class="container-fluid">
            <div class="row" data-container="container">


                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aasa" data-bg-position="left top">


                    <div data-widget-id="wid_1587314132_chb0dimxm" class="moto-widget moto-widget-image moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="image">
                        <span class="moto-widget-image-link">
                            <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-about-img.jpg" class="moto-widget-image-picture lazyloaded" data-id="172" title="" alt="" src="https://template100370.motopreview.com/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-about-img.jpg">
                        </span>
                    </div>
                </div>
                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aasa" data-bg-position="left top">


                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" data-visible-on="-" data-animation="" data-draggable-disabled="">
                        <div class="moto-widget-text-content moto-widget-text-editable" aria-multiline="true">
                            <h2 class="moto-text_system_3">We always <span class="moto-color1_3">work on</span> the conscience.</h2>
                            <p class="moto-text_normal"><br></p>
                            <p class="moto-text_normal">Each our employee loves what he or she does. That’s why our services are the best for any despatch goal.</p>
                        </div>
                    </div>
                    <div id="wid_1587314301_mzdu297m5" class="moto-widget moto-widget-accordion moto-widget_interactive moto-preset-default  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto   moto-disabling_row-fixed set-row-gutter-0" data-widget="accordion" data-preset="default" data-close-others="">
                        <div id="wid_1587314301_mzdu297m5__content" class="moto-widget-accordion__wrapper">
                            <div id="wid_1587314301_q1axrn6gy" data-parent-id="wid_1587314301_mzdu297m5" class="moto-widget moto-widget-accordion__item moto-widget_with-content-editor moto-widget-accordion__item_collapse-icon-right moto-widget-accordion__item_open" data-widget="accordion.item">
                                <div class="moto-widget-accordion__header" data-widget-id="wid_1587314301_q1axrn6gy">
                                    <span class="moto-widget-accordion__header-item moto-widget-accordion__header-title">We Are Reliable</span>
                                    <span class="moto-widget-accordion__header-item moto-widget-accordion__collapse-icon fa fa-fw" aria-hidden="true"></span>
                                </div>
                                <div class="moto-widget__content-wrapper moto-widget-accordion__content-wrapper" style="display: block;">
                                    <div id="wid_1587314301_q1axrn6gy__content" class="moto-widget__content moto-widget-accordion__content ">
                                        <div data-widget-id="wid_1587314301_xmmqd0j4w" class="moto-widget moto-widget-text moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v" data-animation="">
                                            <div class="moto-widget-text-content moto-widget-text-editable">
                                                <p class="moto-text_normal">We appreciate your time and provide quick and efficient services for our clients. Our working plan is organized and always innovated with a growth of technologies. Our educated couriers deliver your cargo in the right place at the right time safely.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="wid_1587314301_85hil2nxd" data-parent-id="wid_1587314301_mzdu297m5" class="moto-widget moto-widget-accordion__item moto-widget_with-content-editor moto-widget-accordion__item_collapse-icon-right moto-entertainment__playing_disabled" data-widget="accordion.item">
                                <div class="moto-widget-accordion__header" data-widget-id="wid_1587314301_85hil2nxd">
                                    <span class="moto-widget-accordion__header-item moto-widget-accordion__header-title">Fast Work</span>
                                    <span class="moto-widget-accordion__header-item moto-widget-accordion__collapse-icon fa fa-fw" aria-hidden="true"></span>
                                </div>
                                <div class="moto-widget__content-wrapper moto-widget-accordion__content-wrapper" style="display: none;">
                                    <div id="wid_1587314301_85hil2nxd__content" class="moto-widget__content moto-widget-accordion__content moto-entertainment__animation_disabled moto-entertainment__animation_ignore-visibility">
                                        <div data-widget-id="wid_1587314301_ipyj8mcpg" class="moto-widget moto-widget-text moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v" data-animation="">
                                            <div class="moto-widget-text-content moto-widget-text-editable">
                                                <p class="moto-text_normal">We appreciate your time and provide quick and efficient services for our clients. Our working plan is organized and always innovated with a growth of technologies. Our educated couriers deliver your cargo in the right place at the right time safely.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="wid_1587314301_qf7ykxjk8" data-parent-id="wid_1587314301_mzdu297m5" class="moto-widget moto-widget-accordion__item moto-widget_with-content-editor moto-widget-accordion__item_collapse-icon-right moto-entertainment__playing_disabled" data-widget="accordion.item">
                                <div class="moto-widget-accordion__header" data-widget-id="wid_1587314301_qf7ykxjk8">
                                    <span class="moto-widget-accordion__header-item moto-widget-accordion__header-title">We Guarantee The Quality Of Transportation</span>
                                    <span class="moto-widget-accordion__header-item moto-widget-accordion__collapse-icon fa fa-fw" aria-hidden="true"></span>
                                </div>
                                <div class="moto-widget__content-wrapper moto-widget-accordion__content-wrapper" style="display: none;">
                                    <div id="wid_1587314301_qf7ykxjk8__content" class="moto-widget__content moto-widget-accordion__content moto-entertainment__animation_disabled moto-entertainment__animation_ignore-visibility">
                                        <div data-widget-id="wid_1587314301_cd1f3lp7i" class="moto-widget moto-widget-text moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v" data-animation="">
                                            <div class="moto-widget-text-content moto-widget-text-editable">
                                                <p class="moto-text_normal">We appreciate your time and provide quick and efficient services for our clients. Our working plan is organized and always innovated with a growth of technologies. Our educated couriers deliver your cargo in the right place at the right time safely.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="moto-widget moto-widget-row row-fixed moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sala" style="" data-bg-position="left top" data-draggable-disabled="">


        <div class="container-fluid">
            <div class="row" data-container="container">







                <div class="moto-widget moto-widget-row__column moto-cell col-sm-8 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                    <div class="moto-widget moto-widget-row moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="aaaa" style="" data-bg-position="left top">


                        <div class="container-fluid">
                            <div class="row" data-container="container">


                                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="sasa" data-bg-position="left top">



                                    <div class="moto-widget moto-widget-container undefined moto-container_content_5e9c94ce1" data-visible-on="" data-widget="container" data-container="container" data-css-name="moto-container_content_5e9c94ce1" data-bg-position="right bottom">




                                        <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-small moto-spacing-bottom-medium moto-spacing-left-small" data-widget="text" data-preset="default" data-spacing="msms" data-visible-on="-" data-animation="">
                                            <div class="moto-widget-text-content moto-widget-text-editable">
                                                <h3 class="moto-text_system_6"><a target="_self" href="/more/" data-action="page" data-id="19" class="moto-link">Cost savings</a></h3>
                                                <p class="moto-text_normal">&nbsp;</p>
                                                <p class="moto-text_normal"><a target="_self" href="/more/" data-action="page" data-id="19" class="moto-link">Save your money with our logistics service taking advantage of quickly delivery.</a></p>
                                            </div>
                                        </div>


                                        <div data-widget-id="wid_1587547512_tsr6r049a" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto moto-visible-on_mobile-h_hidden moto-visible-on_mobile-v_hidden" data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet">
                                            <div class="moto-widget-spacer-block" style="height:22px"></div>
                                        </div>
                                    </div>


                                </div>




                                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="sasa" data-bg-position="left top">






                                    <div class="moto-widget moto-widget-container undefined moto-container_content_5e9c94ce3" data-visible-on="" data-widget="container" data-container="container" data-css-name="moto-container_content_5e9c94ce3" data-bg-position="right bottom" data-draggable-disabled="">




                                        <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-small moto-spacing-bottom-medium moto-spacing-left-small" data-widget="text" data-preset="default" data-spacing="msms" data-visible-on="-" data-animation="" data-draggable-disabled="">
                                            <div class="moto-widget-text-content moto-widget-text-editable" aria-multiline="true">
                                                <h3 class="moto-text_system_6"><a target="_self" href="/more/" data-action="page" data-id="19" class="moto-link">Secure services</a></h3>
                                                <p class="moto-text_normal"><br></p>
                                                <p class="moto-text_normal"><a target="_self" href="/more/" data-action="page" data-id="19" class="moto-link">We provide security of your delivery. You can use our online tracking system to check where your cargo is.</a></p>
                                            </div>
                                        </div>


                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>



                    <div class="moto-widget moto-widget-row moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="aaaa" style="" data-bg-position="left top" data-draggable-disabled="">


                        <div class="container-fluid">
                            <div class="row" data-container="container">


                                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="sasa" data-bg-position="left top">



                                    <div class="moto-widget moto-widget-container undefined moto-container_content_5e9c94ce2" data-visible-on="" data-widget="container" data-container="container" data-css-name="moto-container_content_5e9c94ce2" data-bg-position="right bottom">




                                        <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-small moto-spacing-bottom-medium moto-spacing-left-small" data-widget="text" data-preset="default" data-spacing="msms" data-visible-on="-" data-animation="">
                                            <div class="moto-widget-text-content moto-widget-text-editable">
                                                <h3 class="moto-text_system_6"><a target="_self" href="/more/" data-action="page" data-id="19" class="moto-link">Cargo express</a></h3>
                                                <p class="moto-text_normal">&nbsp;</p>
                                                <p class="moto-text_normal"><a target="_self" href="/more/" data-action="page" data-id="19" class="moto-link">Our educated couriers deliver your cargo in the right place at the right time safely.</a></p>
                                            </div>
                                        </div>


                                        <div data-widget-id="wid_1587547519_6ld12ui6o" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto moto-visible-on_tablet_hidden moto-visible-on_mobile-h_hidden moto-visible-on_mobile-v_hidden" data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop">
                                            <div class="moto-widget-spacer-block" style="height:22px"></div>
                                        </div>
                                    </div>


                                </div>




                                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="sasa" data-bg-position="left top">



                                    <div class="moto-widget moto-widget-container undefined moto-container_content_5e9c94ce4" data-visible-on="" data-widget="container" data-container="container" data-css-name="moto-container_content_5e9c94ce4" data-bg-position="right bottom">




                                        <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-small moto-spacing-bottom-medium moto-spacing-left-small" data-widget="text" data-preset="default" data-spacing="msms" data-visible-on="-" data-animation="">
                                            <div class="moto-widget-text-content moto-widget-text-editable">
                                                <h3 class="moto-text_system_6"><a target="_self" href="/more/" data-action="page" data-id="19" class="moto-link">Proven&nbsp;Experience</a></h3>
                                                <p class="moto-text_normal">&nbsp;</p>
                                                <p class="moto-text_normal"><a target="_self" href="/more/" data-action="page" data-id="19" class="moto-link">Thousands of happy clients, hundreds of permanent customers are the result of our work.</a></p>
                                            </div>
                                        </div>


                                    </div>


                                </div>



                            </div>
                        </div>
                    </div>
                </div>

                <div class="moto-widget moto-widget-row__column moto-cell col-sm-4 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">

                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sasa" data-visible-on="-" data-animation="">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <h2 class="moto-text_system_3">We work quickly and <span class="moto-color1_3">efficiently!</span></h2>
                            <p class="moto-text_normal">&nbsp;</p>
                            <p class="moto-text_normal"><span class="moto-color3_3">We provide local and international delivery services. Machiter is number one company implementing logistics services. With our rich experience we have unique skills and professional staff.</span></p>
                            <p class="moto-text_normal">&nbsp;</p>
                            <p class="moto-text_normal"><span class="moto-color3_3">Working closely with our clients to customize their exact needs has always been very rewarding for us.</span></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>