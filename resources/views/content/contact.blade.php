<section id="section-content" class="content page-15 moto-section" data-widget="section" data-container="section">
    <div class="moto-widget moto-widget-row row-fixed moto-bg-color5_4 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-justify-content_center" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sasa" style="" data-bg-position="left top">


        <div class="container-fluid">
            <div class="row" data-container="container">


                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="-" data-animation="">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <h1 class="moto-text_system_9">Liên hệ</h1>
                        </div>
                    </div>


                </div>


                <div class="moto-widget moto-widget-row__column moto-cell col-sm-6 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                    <div id="wid_1587325197_t5ah8y58r" class="moto-widget moto-widget-breadcrumbs moto-align-right moto-align-left_mobile-h moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto   moto-preset-default" data-widget="breadcrumbs" data-preset="default">
                        <div class="moto-widget-breadcrumbs__wrapper">
                            <ul class="moto-widget-breadcrumbs__list">
                                <li class="moto-widget-breadcrumbs__item">
                                    <a href="/" class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_link"><span class="moto-widget-breadcrumbs__item-text">Trang chủ</span></a>
                                </li>
                                <li class="moto-widget-breadcrumbs__item">
                                    <span class="moto-widget-breadcrumbs__item-delimiter">/</span>
                                    <span class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_current"><span class="moto-widget-breadcrumbs__item-text">Liên hệ</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>



            </div>
        </div>
    </div>
    <div class="moto-widget moto-widget-row row-fixed moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="lama" style="" data-bg-position="left top">


        <div class="container-fluid">
            <div class="row" data-container="container">


                <div class="moto-widget moto-widget-row__column moto-cell col-sm-12 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                    <div class="moto-widget moto-widget-row moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sama" style="" data-bg-position="left top">


                        <div class="container-fluid">
                            <div class="row" data-container="container">


                                <div class="moto-widget moto-widget-row__column moto-cell col-sm-4 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="saaa" data-bg-position="left top">


                                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" data-visible-on="-" data-animation="">
                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                            <p class="moto-text_system_6">Địa chỉ:</p>
                                            <p class="moto-text_system_2">&nbsp;</p>
                                            <p class="moto-text_normal"><span class="fa"></span>&nbsp; Trụ sở chính: Số 288 Đường Hoàng Mai, Tổ 37, Phường Hoàng Văn Thụ, Quận Hoàng Mai, TP Hà Nội.</p>
                                            <p class="moto-text_normal"><span class="fa"></span>&nbsp; Văn phòng giao dịch: Số 5, ngách 48, ngõ 49 Thúy Lĩnh, Lĩnh Nam, Hoàng Mai, Hà Nội</p>
                                        </div>
                                    </div>
                                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sasa" data-visible-on="-" data-animation="">
                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                            <p class="moto-text_system_6">Số điện thoại:</p>
                                            <p class="moto-text_system_2">&nbsp;</p>
                                            <p class="moto-text_system_7"><span class="moto-color2_3"><span class="fa"></span>&nbsp; &nbsp;</span><a data-action="call" class="moto-link" href="tel:0968 749 000">0968 749 000</a></p>
                                            <!-- <p class="moto-text_system_7"><span class="fa"></span>&nbsp;&nbsp;<a data-action="call" class="moto-link" href="tel:123-456-7890">123-456-7891</a></p> -->
                                        </div>
                                    </div>
                                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="saaa" data-visible-on="-" data-animation="">
                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                            <p class="moto-text_system_6">E-mail:</p>
                                            <p class="moto-text_system_2">&nbsp;</p>
                                            <p class="moto-text_system_7"><span class="moto-color2_3"><span class="fa"></span>&nbsp;&nbsp;</span><a data-action="mail" class="moto-link" href="mailto:xuantruonglogistics@gmail.com">xuantruonglogistics@gmail.com</a></p>
                                            <!-- <p class="moto-text_system_7">Download information as: <a target="_self" data-action="url" class="moto-link" href="#">vCard</a></p> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-row__column moto-cell col-sm-8 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="saaa" data-bg-position="left top">


                                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" data-visible-on="-" data-animation="">
                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                            <p class="moto-text_system_6">Gửi tin nhắn:</p>
                                            <p class="moto-text_normal">&nbsp;</p>
                                            <p class="moto-text_normal">Gửi mail cho chúng tôi nếu có bất kỳ câu hỏi hoặc thắc mắc nào.</p>
                                        </div>
                                    </div>
                                    <div data-widget-id="wid_1587326101_up0r6aqjf" class="moto-widget moto-widget-contact_form moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-preset="default" data-widget="contact_form" data-spacing="saaa">
                                        <div ng-controller="widget.ContactForm.Controller" ng-init="hash = '2@eyJoIjoiSDRIQVJIczZMQ2hqUFNJUWQydWRiXC9OK1lIVnpId3RSWUJXWnFKNkxKdDg9IiwiaSI6ImFidHBcL3grazBzZ1Bkb2RNSGlWajFRPT0iLCJ2IjoiVUsrS0k2ZmFNMjFNcXlcL2JmVU5GYjZGVmRvMno4TlFIK1hWR0JvdXZZMlZqN2ZCdWJUTWYyWGtLK2hyeUtRZHFTQmtrQkNqWTJrOStVUlwvN0ZkQyttQT09In0=';actionAfterSubmission={&quot;action&quot;:&quot;none&quot;,&quot;url&quot;:&quot;&quot;,&quot;target&quot;:&quot;_self&quot;,&quot;id&quot;:&quot;&quot;};resetAfterSubmission=false">
                                            <!-- <form class="moto-widget-contact_form-form ng-pristine ng-invalid ng-invalid-required" role="form" name="contactForm" ng-submit="submit()" novalidate="">
                                                <div ng-show="sending" class="contact-form-loading ng-hide"></div>

                                                <div class="moto-widget-contact_form-group">
                                                    <label for="field_name_wid_1587326101_up0r6aqjf" class="moto-widget-contact_form-label">Name</label>
                                                    <input type="text" class="moto-widget-contact_form-field moto-widget-contact_form-input ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" placeholder="Name *" ng-blur="validate('name')" required="" ng-model-options="{ updateOn: 'blur' }" name="name" id="field_name_wid_1587326101_up0r6aqjf" ng-model="message.name">
                                                    <span class="moto-widget-contact_form-field-error ng-hide" ng-show="contactForm.name.$invalid &amp;&amp; !contactForm.name.$pristine &amp;&amp; !contactForm.name.emailInvalid">Field is required</span>
                                                </div>

                                                <div class="moto-widget-contact_form-group">
                                                    <label for="field_email_wid_1587326101_up0r6aqjf" class="moto-widget-contact_form-label">Email</label>
                                                    <input type="text" class="moto-widget-contact_form-field moto-widget-contact_form-input ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" placeholder="Email *" ng-blur="validate('email')" required="" ng-model-options="{ updateOn: 'blur' }" name="email" id="field_email_wid_1587326101_up0r6aqjf" ng-model="message.email">
                                                    <span class="moto-widget-contact_form-field-error ng-hide" ng-show="contactForm.email.$invalid &amp;&amp; !contactForm.email.$pristine &amp;&amp; !contactForm.email.emailInvalid">Field is required</span>
                                                    <span class="moto-widget-contact_form-field-error ng-hide" ng-show="contactForm.email.emailInvalid &amp;&amp; !contactForm.email.$pristine">Incorrect email</span>
                                                </div>

                                                <div class="moto-widget-contact_form-group">
                                                    <label for="field_phone_wid_1587326101_up0r6aqjf" class="moto-widget-contact_form-label">Phone</label>
                                                    <input type="text" class="moto-widget-contact_form-field moto-widget-contact_form-input ng-pristine ng-untouched ng-valid ng-empty" placeholder="Phone " ng-model-options="{ updateOn: 'blur' }" name="phone" id="field_phone_wid_1587326101_up0r6aqjf" ng-model="message.phone">
                                                    <span class="moto-widget-contact_form-field-error ng-hide" ng-show="contactForm.phone.$invalid &amp;&amp; !contactForm.phone.$pristine &amp;&amp; !contactForm.phone.emailInvalid">Field is required</span>
                                                </div>


                                                <div class="moto-widget-contact_form-group">
                                                    <label for="field_message_wid_1587326101_up0r6aqjf" class="moto-widget-contact_form-label">Message</label>
                                                    <textarea class="moto-widget-contact_form-field moto-widget-contact_form-textarea ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" rows="3" placeholder="Message *" ng-blur="validate('message')" required="" ng-model-options="{ updateOn: 'blur' }" name="message" id="field_message_wid_1587326101_up0r6aqjf" ng-model="message.message"></textarea>
                                                    <span class="moto-widget-contact_form-field-error ng-hide" ng-show="contactForm.message.$invalid &amp;&amp; !contactForm.message.$pristine">Field is required</span>
                                                </div>



                                                <div class="moto-widget-contact_form-success ng-hide" ng-show="showSuccessMessage">
                                                    Your message was sent successfully
                                                </div>
                                                <div class="moto-widget-contact_form-danger ng-hide" ng-show="emailError">
                                                    Sorry, your message was not sent
                                                </div>
                                                <div class="moto-widget-contact_form-buttons">

                                                    <div class="moto-widget moto-widget-button moto-preset-3 moto-align-left" data-preset="3" data-align="left">
                                                        <a ng-click="submit();" class="moto-widget-button-link moto-size-medium" data-size="medium"><span class="fa moto-widget-theme-icon"></span><span class="moto-widget-button-label">SEND</span></a>
                                                    </div>
                                                    <button type="submit" class="hidden"></button>

                                                </div>
                                            </form> -->
                                            <form id="contactForm" class="moto-widget-contact_form-form ng-pristine ng-invalid ng-invalid-required" role="form" name="contactForm" ng-submit="submit()" novalidate="">
                                                {{csrf_field()}}
                                                <div ng-show="sending" class="contact-form-loading ng-hide"></div>

                                                <div class="moto-widget-contact_form-group">
                                                    <label for="field_name_wid_1587241212_kth75dmq6" class="moto-widget-contact_form-label">Tên</label>
                                                    <input type="text" class="moto-widget-contact_form-field moto-widget-contact_form-input ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" placeholder="Tên *" ng-blur="validate('name')" required="" ng-model-options="{ updateOn: 'blur' }" name="name" id="name" ng-model="message.name">
                                                    <span class="moto-widget-contact_form-field-error ng-hide" id="errorName">Vui lòng điền vào trường này</span>
                                                </div>

                                                <div class="moto-widget-contact_form-group">
                                                    <label for="field_phone_wid_1587241212_kth75dmq6" class="moto-widget-contact_form-label">Điện thoại</label>
                                                    <input type="text" class="moto-widget-contact_form-field moto-widget-contact_form-input ng-pristine ng-untouched ng-valid ng-empty" placeholder="Điện thoại " ng-model-options="{ updateOn: 'blur' }" name="phone" id="phone" ng-model="message.phone">
                                                    <span class="moto-widget-contact_form-field-error ng-hide" id="errorPhone">Vui lòng điền vào trường này</span>
                                                </div>

                                                <div class="moto-widget-contact_form-group">
                                                    <label for="field_email_wid_1587241212_kth75dmq6" class="moto-widget-contact_form-label">Email</label>
                                                    <input type="text" class="moto-widget-contact_form-field moto-widget-contact_form-input ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" placeholder="Email *" ng-blur="validate('email')" required="" ng-model-options="{ updateOn: 'blur' }" name="email" id="email" ng-model="message.email">
                                                    <span class="moto-widget-contact_form-field-error ng-hide" id="errorEmail">Vùi lòng điền vào trường này</span>
                                                    <span class="moto-widget-contact_form-field-error ng-hide" id="errorEmailFormat">Định dạng mail không đúng</span>
                                                </div>




                                                <div class="moto-widget-contact_form-group">
                                                    <label for="field_message_wid_1587241212_kth75dmq6" class="moto-widget-contact_form-label">Tin nhắn</label>
                                                    <textarea maxlength="500" class="moto-widget-contact_form-field moto-widget-contact_form-textarea ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" rows="3" placeholder="Tin nhắn *" ng-blur="validate('message')" required="" ng-model-options="{ updateOn: 'blur' }" name="message" id="message" ng-model="message.message"></textarea>
                                                    <span class="moto-widget-contact_form-field-error ng-hide" id="errorMessage">Vui lòng điền vào trường này</span>
                                                </div>



                                                <div id="messageSuccess" class="moto-widget-contact_form-success ng-hide">
                                                    Tin nhắn của bạn được gửi thành công
                                                </div>
                                                <div id="messageError" class="moto-widget-contact_form-danger ng-hide">
                                                    Tin nhắn của bạn gửi không thành công
                                                </div>
                                                <div class="moto-widget-contact_form-buttons">

                                                    <div class="moto-widget moto-widget-button moto-preset-3 moto-align-left" data-preset="3" data-align="left">
                                                        <button id="submit" type="button" class="moto-widget-button-link moto-size-medium" data-size="medium"><span class="fa moto-widget-theme-icon"></span><span class="moto-widget-button-label">Gửi tin nhắn</span></button>
                                                    </div>
                                                    <button type="submit" class="hidden"></button>

                                                </div>
                                            </form>
                                            <script>
                                                jQuery(document).ready(function() {
                                                    console.log('ready');
                                                    $('#submit').click(function(e) {
                                                        e.preventDefault();
                                                        removeMessage();
                                                        let formData = $('#contactForm').serializeArray();
                                                        var validateForm = validate();
                                                        if (validateForm) {
                                                            $('#submit').prop('disabled', true);
                                                            $.ajax({
                                                                method: "POST",
                                                                headers: {
                                                                    Accept: "application/json"
                                                                },
                                                                url: "{{ route('message') }}",
                                                                data: formData,
                                                                success: () => {
                                                                    $('#messageSuccess').removeClass('ng-hide');
                                                                    $('#submit').prop('disabled', false);
                                                                },
                                                                error: (response) => {
                                                                    $('#messageError').removeClass('ng-hide');
                                                                    $('#submit').prop('disabled', false);
                                                                }
                                                            });
                                                        }
                                                    });
                                                });

                                                function removeMessage() {
                                                    $('#errorName').addClass('ng-hide');
                                                    $('#errorEmailFormat').addClass('ng-hide');
                                                    $('#errorEmail').addClass('ng-hide');
                                                    $('#errorMessage').addClass('ng-hide');
                                                    $('#messageSuccess').addClass('ng-hide');
                                                    $('#messageError').addClass('ng-hide');
                                                }

                                                function validate() {
                                                    var validateForm = true;
                                                    var name = $('#name').val();
                                                    var phone = $('#phone').val();
                                                    var email = $('#email').val();
                                                    var message = $('#message').val();
                                                    if (!name) {
                                                        $('#errorName').removeClass('ng-hide');
                                                        validateForm = false;
                                                    }
                                                    if (!email) {
                                                        $('#errorEmailFormat').addClass('ng-hide');
                                                        $('#errorEmail').removeClass('ng-hide');
                                                        validateForm = false;
                                                    } else {
                                                        if (!validateEmail(email)) {
                                                            $('#errorEmail').addClass('ng-hide');
                                                            $('#errorEmailFormat').removeClass('ng-hide');
                                                            validateForm = false;
                                                        }
                                                    }
                                                    if (!message) {
                                                        $('#errorMessage').removeClass('ng-hide');
                                                        validateForm = false;
                                                    }
                                                    return validateForm;
                                                }

                                                function validateEmail(email) {
                                                    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                                    return re.test(email);
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</section>