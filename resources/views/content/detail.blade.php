<div class="moto-widget moto-widget-row row-fixed moto-justify-content_center moto-bg-color5_4 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sasa" style="" data-bg-position="left top">


    <div class="container-fluid">
        <div class="row" data-container="container">


            <div class="moto-widget moto-widget-row__column moto-cell col-sm-3 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="-" data-animation="">
                    <div class="moto-widget-text-content moto-widget-text-editable">
                        <h1 class="moto-text_system_9">ONE OF THE POST</h1>
                    </div>
                </div>


            </div>




            <div class="moto-widget moto-widget-row__column moto-cell col-sm-9 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                <div id="wid_1587405476_yig24n4y2" class="moto-widget moto-widget-breadcrumbs moto-align-right moto-align-left_mobile-v moto-align-left_mobile-h moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto   moto-preset-default" data-widget="breadcrumbs" data-preset="default">
                    <div class="moto-widget-breadcrumbs__wrapper">
                        <ul class="moto-widget-breadcrumbs__list">
                            <li class="moto-widget-breadcrumbs__item">
                                <a href="/" class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_link"><span class="moto-widget-breadcrumbs__item-text">Home</span></a>
                            </li>
                            <li class="moto-widget-breadcrumbs__item">
                                <span class="moto-widget-breadcrumbs__item-delimiter">/</span>
                                <a href="/blog/" class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_link"><span class="moto-widget-breadcrumbs__item-text">Blog Main Page</span></a>
                            </li>
                            <li class="moto-widget-breadcrumbs__item">
                                <span class="moto-widget-breadcrumbs__item-delimiter">/</span>
                                <a href="/blog/category/recent/" class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_link"><span class="moto-widget-breadcrumbs__item-text">Recent</span></a>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>



        </div>
    </div>
</div>

<div class="moto-widget moto-widget-row row-fixed moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-widget="row" style="" data-bg-position="left top" data-spacing="lala" data-visible-on="-">
    <div class="container-fluid">
        <div class="row" data-container="container">
            <div class="moto-cell col-sm-9 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                <div data-widget-id="wid__blog_post_name__5e9de29946060" class="moto-widget moto-widget-blog-post_name moto-preset-default moto-align-left moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-preset="default" data-widget="blog.post_name">
                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <h1 class="moto-text_system_6">The most common mistakes when managing personal finances</h1>
                        </div>
                    </div>
                </div>
                <div data-grid-type="sm" class="moto-widget moto-widget-row row-gutter-0" data-widget="row" style="" data-bg-position="left top">
                    <div class="container-fluid">
                        <div class="row" data-container="container">
                            <div class="moto-cell col-sm-2 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                <div data-widget-id="wid__blog_post_published_on__5e9de29946551" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                    <div class="moto-text_system_11">
                                        <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                            20.04.2020
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="moto-cell col-sm-10 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                <div class="moto-widget moto-widget-row moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="aaaa" style="" data-bg-position="left top">


                                    <div class="container-fluid">
                                        <div class="row" data-container="container">


                                            <div class="moto-widget moto-widget-row__column moto-cell col-sm-3 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">
                                                <div data-widget-id="wid__blog_post_author__5e9de29946a6e" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                    <div class="moto-text_system_11">
                                                        <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Demo User</span>
                                                    </div>
                                                </div>


                                            </div>




                                            <div class="moto-widget moto-widget-row__column moto-cell col-sm-9 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                                                <div data-widget-id="wid_1587405716_qp1zek8f1" class="moto-widget moto-widget-blog-post_category moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left " data-widget="blog.post_category">
                                                    <div class="moto-text_system_11">
                                                        <a href="/blog/category/recent/">
                                                            <span class="fa fa-folder moto-widget-blog-post_category-icon" aria-hidden="true"></span><span class="moto-widget-blog-post_category-text">Recent</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-widget-id="wid_1587405465_vzx3at259" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                    <div class="moto-widget-spacer-block" style="height:10px"></div>
                </div>
                <div data-widget-id="wid__social_buttons__5e9de2994702f" class="moto-widget moto-widget-social-buttons moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="social_buttons" data-preset="default" data-spacing="aasa">

                    <ul class="social-buttons-list">
                        <li class="social-button" data-name="facebook_share" data-provider="facebook">
                            <div class="fb-share-button fb_iframe_widget" data-href="https://template100370.motopreview.com/blog/the-most-common-mistakes-when-managing-personal-finances/" data-layout="button_count" moto-dependency-require="facebook" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=0&amp;href=https%3A%2F%2Ftemplate100370.motopreview.com%2Fblog%2Fthe-most-common-mistakes-when-managing-personal-finances%2F&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey"><span style="vertical-align: bottom; width: 77px; height: 20px;"><iframe name="f319ddd3110a418" width="1000px" height="1000px" data-testid="fb:share_button Facebook Social Plugin" title="fb:share_button Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.8/plugins/share_button.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Dff1b7e2d68895c%26domain%3Dtemplate100370.motopreview.com%26origin%3Dhttps%253A%252F%252Ftemplate100370.motopreview.com%252Ff260ee5989becb4%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Ftemplate100370.motopreview.com%2Fblog%2Fthe-most-common-mistakes-when-managing-personal-finances%2F&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey" style="border: none; visibility: visible; width: 77px; height: 20px;" class=""></iframe></span></div>
                        </li>
                        <li class="social-button" data-name="twitter_tweet" data-provider="twitter"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" style="position: static; visibility: visible; width: 60px; height: 20px;" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.3c5aa8e2a38bbbee4b6d88e6846fc657.en.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=https%3A%2F%2Ftemplate100370.motopreview.com%2Fblog%2Fthe-most-common-mistakes-when-managing-personal-finances%2F&amp;size=m&amp;text=The%20most%20common%20mistakes%20when%20managing%20personal%20finances&amp;time=1598236825853&amp;type=share&amp;url=https%3A%2F%2Ftemplate100370.motopreview.com%2Fblog%2Fthe-most-common-mistakes-when-managing-personal-finances%2F"></iframe></li>
                        <li class="social-button" data-name="linkedIn_share" data-provider="linkedin"><span class="IN-widget" data-lnkd-debug="<script type=&quot;IN/Share+init&quot; data-url=&quot;https://template100370.motopreview.com/blog/the-most-common-mistakes-when-managing-personal-finances/&quot; data-counter=&quot;right&quot; moto-dependency-require=&quot;linkedin&quot;></script>" style="display: inline-block; line-height: 1; vertical-align: bottom; padding: 0px; margin: 0px; text-indent: 0px; text-align: center;"><span style="padding: 0px !important; margin: 0px !important; text-indent: 0px !important; display: inline-block !important; vertical-align: bottom !important; font-size: 1px !important;"><button class="IN-2bc0215c-7188-4274-b598-1969e06d4d7c-1G9ISYhSF8XoOmdcl0yKDu">
                                        <xdoor-icon aria-hidden="true"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet">
                                                <g style="fill: currentColor">
                                                    <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
                                                    <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
                                                </g>
                                            </svg></xdoor-icon>Share
                                    </button></span></span></li>
                    </ul>
                </div>
                <div data-widget-id="wid_1587405465_rgplr1d3h" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                    <div class="moto-widget-spacer-block" style="height:10px"></div>
                </div>
                <div data-widget-id="wid__blog_post_content__5e9de29947799" class="moto-widget moto-widget-blog-post_content moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="blog.post_content">

                    <section id="section-content" class="content page-25 moto-section" data-widget="section" data-container="section">
                        <div class="moto-widget moto-widget-row" data-widget="row" style="" data-bg-position="left top" data-draggable-disabled="">
                            <div class="container-fluid">
                                <div class="row" data-container="container">
                                    <div class="moto-cell col-sm-12 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                        <div data-widget-id="wid__image__5e9dd4ed14576" class="moto-widget moto-widget-image moto-preset-4 moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                            <span class="moto-widget-image-link">
                                                <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-1.jpg" class="moto-widget-image-picture lazyloaded" data-id="174" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-1.jpg">
                                            </span>
                                        </div>
                                        <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sala" data-visible-on="-" data-animation="" data-draggable-disabled="">
                                            <div class="moto-widget-text-content moto-widget-text-editable" aria-multiline="true">
                                                <p class="moto-text_normal">The ability to manage money competently is especially valuable quality in the conditions of financial crisis, when the purchasing power of the population is shrinking, inflation is rising, and currency exchange rates are completely unpredictable. Below are the common mistakes related to money affairs along with financial planning advice to help manage your own finances properly.</p>
                                                <p class="moto-text_normal"><br></p>
                                                <p class="moto-text_normal">The budget is the most basic thing in financial planning. It is therefore especially important to be careful when compiling the budget. To start you have to draw up your own budget for the next month and only after it you may make a yearly budget.</p>
                                                <p class="moto-text_normal"><br></p>
                                                <p class="moto-text_normal">As the basis takes your monthly income, subtract from it such regular expenses as the cost of housing, transportation, and then select 20-30% on savings or mortgage loan payment.</p>
                                                <p class="moto-text_normal">The rest can be spent on living: restaurants, entertainment, etc. If you are afraid of spending too much, limit yourself in weekly expenses by having a certain amount of ready cash.</p>
                                                <p class="moto-text_normal"><br></p>
                                                <p class="moto-text_normal">"When people borrow, they think that they should return it as soon as possible," said Sofia Bera, a certified financial planner and founder of Gen Y Planning company. And at its repayment spend all that earn. But it's not quite rationally ".</p>
                                                <p class="moto-text_normal"><br></p>
                                                <p class="moto-text_normal">If you don't have money on a rainy day, in case of an emergency (e.g. emergency of car repairs) you have to pay by credit card or get into new debts. Keep on account of at least $1000 in case of unexpected expenses. And gradually increase the "airbag" to an amount equal to your income for up to three-six months.</p>
                                                <p class="moto-text_normal"><br></p>
                                                <p class="moto-text_normal">"Usually when people plan to invest, they only think about profit and they don't think that loss's possible", says Harold Evensky, the President of the financial management company Evensky &amp; Katz. He said that sometimes people do not do basic mathematical calculations.</p>
                                                <p class="moto-text_normal">For example, forgetting that if in one year they lost 50%, and the following year they received 50% of the profits, they did not return to the starting point, and lost 25% savings. Therefore, think about the consequences. Get ready to any options. And of course, it would be wiser to invest in several different investment objects.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
                <div data-widget-id="wid_1587406098_2kp7kmf9o" class="moto-widget moto-widget-image moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="image">
                    <span class="moto-widget-image-link">
                        <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png" class="moto-widget-image-picture lazyloaded" data-id="195" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png">
                    </span>
                </div>
                <div data-widget-id="wid__blog_post_comments__5e9de29947bd5" class="moto-widget moto-widget-blog-post_comments moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_comments" data-preset="default">
                    <div class="moto-widget-empty"></div>
                    <div data-widget-id="wid__disqus__5f4328440d699" class="moto-widget moto-widget-disqus moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="disqus" data-params="{&quot;shortname&quot;:null,&quot;language&quot;:&quot;en&quot;,&quot;use_identifier&quot;:false,&quot;identifier&quot;:&quot;&quot;,&quot;url&quot;:&quot;@dynamic&quot;}" data-url="https://template100370.motopreview.com/blog/the-most-common-mistakes-when-managing-personal-finances/">
                        <div class="moto-widget-empty"></div>
                    </div>
                </div>
            </div>
            <div class="moto-cell col-sm-3 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">

                <div data-widget-id="wid_1587405548_l43cj90lb" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                    <div class="moto-widget-spacer-block" style="height:10px"></div>
                </div>
                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" data-visible-on="-" data-animation="">
                    <div class="moto-widget-text-content moto-widget-text-editable">
                        <p class="moto-text_system_5">Categories</p>
                    </div>
                </div>
                <div data-widget-id="wid_1587405548_2vua12o3f" class="moto-widget moto-widget-blog-category_list moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto moto-align-left  moto-widget-blog-category_list_show-delimiter moto-widget_flex-alignment" data-widget="blog.category_list" data-preset="default">
                    <div class="moto-widget-blog-category_list__content-wrapper">
                        <ul class="moto-widget-blog-category_list__items">
                            <li class="moto-widget-blog-category_list__item"><a href="/blog/category/archived-publications/" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">Archived publications</span></a></li>
                            <li class="moto-widget-blog-category_list__item"><a href="/blog/category/fresh-topic/" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">Fresh topic</span></a></li>
                            <li class="moto-widget-blog-category_list__item"><a href="/blog/category/information/" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">Information</span></a></li>
                            <li class="moto-widget-blog-category_list__item"><a href="/blog/category/latest/" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">Latest</span></a></li>
                            <li class="moto-widget-blog-category_list__item"><a href="/blog/category/news/" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">News</span></a></li>
                            <li class="moto-widget-blog-category_list__item"><a href="/blog/category/recent/" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">Recent</span></a></li>
                            <li class="moto-widget-blog-category_list__item"><a href="/blog/category/upcoming-events/" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">Upcoming events</span></a></li>
                            <li class="moto-widget-blog-category_list__item"><a href="/blog/category/updates/" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">Updates</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sasa" data-visible-on="-" data-animation="">
                    <div class="moto-widget-text-content moto-widget-text-editable">
                        <p class="moto-text_system_5">Tags</p>
                    </div>
                </div>
                <div data-widget-id="wid_1587405633_9j876s966" class="moto-widget moto-widget-blog-post_tags moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="blog.post_tags" data-preset="default">
                    <div class="moto-widget-blog-post_tags__content-wrapper">
                        <ul class="moto-widget-blog-post_tags__items">
                            <li class="moto-widget-blog-post_tags__item"><a href="/blog/tag/fresh/" class="moto-widget-blog-post_tags__item-link"><span class="moto-widget-blog-post_tags__item-text">fresh</span></a></li>
                            <li class="moto-widget-blog-post_tags__item"><a href="/blog/tag/hot/" class="moto-widget-blog-post_tags__item-link"><span class="moto-widget-blog-post_tags__item-text">hot</span></a></li>
                        </ul>
                    </div>
                </div>
                <div data-widget-id="wid_1587405548_94weynsk2" class="moto-widget moto-widget-blog-recent_posts moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="blog.recent_posts">
                    <div class="moto-widget-blog-recent_posts-title">
                        <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                            <div class="moto-widget-text-content moto-widget-text-editable">
                                <p class="moto-text_system_5">Recent Posts</p>
                            </div>
                        </div>
                    </div>

                    <div class="moto-widget moto-widget-row" data-widget="row">
                        <div class="container-fluid">
                            <div class="row moto-widget-blog-recent_posts-list">

                                <div class="moto-cell col-sm-12 moto-widget-blog-recent_posts-item">
                                    <div class="moto-widget-blog-recent_posts-item__content  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto">

                                        <div class="moto-widget-blog-recent_posts-item-preview">
                                            <div data-widget-id="wid__image__5f4328440f8c1" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                <a href="https://template100370.motopreview.com/blog/the-most-common-mistakes-when-managing-personal-finances/" data-action="url" class="moto-widget-image-link moto-link">
                                                    <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-1.jpg" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-1.jpg">
                                                </a>
                                            </div>
                                        </div>

                                        <div class="moto-widget-blog-recent_posts-item-title">
                                            <div class="moto-widget moto-widget-text moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                                <div class="moto-widget-text-content moto-widget-text-editable">
                                                    <h2 class="blog-post-title moto-text_system_2">
                                                        <a href="https://template100370.motopreview.com/blog/the-most-common-mistakes-when-managing-personal-finances/">The most common mistakes when managing personal finances</a>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="moto-widget moto-widget-row" data-widget="row">
                                            <div class="container-fluid">
                                                <div class="row">

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_published_on__5f4328440f9b8" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                    20.04.2020
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_author__5f4328440fa2a" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Demo User</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>


                                <div class="moto-cell col-sm-12 moto-widget-blog-recent_posts-item">
                                    <div class="moto-widget-blog-recent_posts-item__content  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto">

                                        <div class="moto-widget-blog-recent_posts-item-preview">
                                            <div data-widget-id="wid__image__5f432844100ca" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                <a href="https://template100370.motopreview.com/blog/methods-of-the-recruitment/" data-action="url" class="moto-widget-image-link moto-link">
                                                    <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-2.jpg" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-2.jpg">
                                                </a>
                                            </div>
                                        </div>

                                        <div class="moto-widget-blog-recent_posts-item-title">
                                            <div class="moto-widget moto-widget-text moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                                <div class="moto-widget-text-content moto-widget-text-editable">
                                                    <h2 class="blog-post-title moto-text_system_2">
                                                        <a href="https://template100370.motopreview.com/blog/methods-of-the-recruitment/">Methods of the recruitment</a>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="moto-widget moto-widget-row" data-widget="row">
                                            <div class="container-fluid">
                                                <div class="row">

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_published_on__5f43284410171" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                    20.04.2020
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_author__5f432844101fd" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Demo User</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>


                                <div class="moto-cell col-sm-12 moto-widget-blog-recent_posts-item">
                                    <div class="moto-widget-blog-recent_posts-item__content  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto">

                                        <div class="moto-widget-blog-recent_posts-item-preview">
                                            <div data-widget-id="wid__image__5f432844107e1" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                <a href="https://template100370.motopreview.com/blog/overalls-with-logo-as-a-method-of-advertising/" data-action="url" class="moto-widget-image-link moto-link">
                                                    <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-4.jpg" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-4.jpg">
                                                </a>
                                            </div>
                                        </div>

                                        <div class="moto-widget-blog-recent_posts-item-title">
                                            <div class="moto-widget moto-widget-text moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                                <div class="moto-widget-text-content moto-widget-text-editable">
                                                    <h2 class="blog-post-title moto-text_system_2">
                                                        <a href="https://template100370.motopreview.com/blog/overalls-with-logo-as-a-method-of-advertising/">Overalls with logo as a method of advertising</a>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="moto-widget moto-widget-row" data-widget="row">
                                            <div class="container-fluid">
                                                <div class="row">

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_published_on__5f43284410883" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                    20.04.2020
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_author__5f432844108e6" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Demo User</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>


                                <div class="moto-cell col-sm-12 moto-widget-blog-recent_posts-item">
                                    <div class="moto-widget-blog-recent_posts-item__content  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto">

                                        <div class="moto-widget-blog-recent_posts-item-preview">
                                            <div data-widget-id="wid__image__5f43284410e92" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                <a href="https://template100370.motopreview.com/blog/the-main-objectives-of-the-marketer/" data-action="url" class="moto-widget-image-link moto-link">
                                                    <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-6.jpg" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-6.jpg">
                                                </a>
                                            </div>
                                        </div>

                                        <div class="moto-widget-blog-recent_posts-item-title">
                                            <div class="moto-widget moto-widget-text moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                                <div class="moto-widget-text-content moto-widget-text-editable">
                                                    <h2 class="blog-post-title moto-text_system_2">
                                                        <a href="https://template100370.motopreview.com/blog/the-main-objectives-of-the-marketer/">The main objectives of the marketer</a>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="moto-widget moto-widget-row" data-widget="row">
                                            <div class="container-fluid">
                                                <div class="row">

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_published_on__5f43284410f29" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                    20.04.2020
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_author__5f43284410f94" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Demo User</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>


                                <div class="moto-cell col-sm-12 moto-widget-blog-recent_posts-item">
                                    <div class="moto-widget-blog-recent_posts-item__content  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto">

                                        <div class="moto-widget-blog-recent_posts-item-preview">
                                            <div data-widget-id="wid__image__5f43284411680" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                <a href="https://template100370.motopreview.com/blog/recession-is-a-good-opportunity-to-deal-a-deathblow-to-the-competitors/" data-action="url" class="moto-widget-image-link moto-link">
                                                    <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-8.jpg" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-8.jpg">
                                                </a>
                                            </div>
                                        </div>

                                        <div class="moto-widget-blog-recent_posts-item-title">
                                            <div class="moto-widget moto-widget-text moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                                <div class="moto-widget-text-content moto-widget-text-editable">
                                                    <h2 class="blog-post-title moto-text_system_2">
                                                        <a href="https://template100370.motopreview.com/blog/recession-is-a-good-opportunity-to-deal-a-deathblow-to-the-competitors/">Recession is a good opportunity to deal a deathblow to the competitors</a>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="moto-widget moto-widget-row" data-widget="row">
                                            <div class="container-fluid">
                                                <div class="row">

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_published_on__5f432844117a8" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                    20.04.2020
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="moto-cell col-sm" data-container="container">
                                                        <div data-widget-id="wid__blog_post_author__5f43284411845" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                            <div class="moto-text_287">
                                                                <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Demo User</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>