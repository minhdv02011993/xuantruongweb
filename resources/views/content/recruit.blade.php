<section id="section-content" class="content page-3 moto-section" data-widget="section" data-container="section">
    <div class="moto-widget moto-widget-row row-fixed moto-justify-content_center moto-bg-color5_4 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sasa" style="" data-bg-position="left top">


        <div class="container-fluid">
            <div class="row" data-container="container">


                <div class="moto-widget moto-widget-row__column moto-cell col-sm-3 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="-" data-animation="">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <h1 class="moto-text_system_9">Tuyển dụng</h1>
                        </div>
                    </div>


                </div>




                <div class="moto-widget moto-widget-row__column moto-cell col-sm-9 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                    <div id="wid_1587402926_p8907vih0" class="moto-widget moto-widget-breadcrumbs moto-align-right moto-align-left_mobile-v moto-align-left_mobile-h moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto   moto-preset-default" data-widget="breadcrumbs" data-preset="default">
                        <div class="moto-widget-breadcrumbs__wrapper">
                            <ul class="moto-widget-breadcrumbs__list">
                                <li class="moto-widget-breadcrumbs__item">
                                    <a href="/" class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_link"><span class="moto-widget-breadcrumbs__item-text">Trang chủ</span></a>
                                </li>
                                <li class="moto-widget-breadcrumbs__item">
                                    <span class="moto-widget-breadcrumbs__item-delimiter">/</span>
                                    <span class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_current"><span class="moto-widget-breadcrumbs__item-text">Tuyển dụng</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>



            </div>
        </div>
    </div>
    <div class="moto-widget moto-widget-row row-fixed moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-widget="row" style="" data-bg-position="left top" data-spacing="lala" data-visible-on="-">
        <div class="container-fluid">
            <div class="row" data-container="container">

                <div class="moto-cell col-sm-9 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                    <div data-widget-id="wid_1587405465_rgplr1d3h" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                        <div class="moto-widget-spacer-block" style="height:10px"></div>
                    </div>
                    <div data-widget-id="wid__blog_post_content__5e9de29947799" class="moto-widget moto-widget-blog-post_content moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="blog.post_content">

                        <section id="section-content" class="content page-25 moto-section" data-widget="section" data-container="section">
                            <div class="moto-widget moto-widget-row" data-widget="row" style="" data-bg-position="left top" data-draggable-disabled="">
                                <div class="container-fluid">
                                    <div class="row" data-container="container">
                                        @if($post)
                                        <div class="moto-cell col-sm-12 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                            <div data-widget-id="wid__blog_post_name__5e9de29946060" class="moto-widget moto-widget-blog-post_name moto-preset-default moto-align-left moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-preset="default" data-widget="blog.post_name">
                                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <h1 class="moto-text_system_6">{{$post->title}}</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-grid-type="sm" class="moto-widget moto-widget-row row-gutter-0" data-widget="row" style="" data-bg-position="left top">
                                                <div class="container-fluid">
                                                    <div class="row" data-container="container">
                                                        <div class="moto-cell col-sm-2 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                                            <div data-widget-id="wid__blog_post_published_on__5e9de29946551" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                                <div class="moto-text_system_11">
                                                                    <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                        {{date('d-m-Y', strtotime($post->created_at))}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-widget-id="wid__blog_post_content__5e9de29947799" class="moto-widget moto-widget-blog-post_content moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="blog.post_content">

                                                <section id="section-content" class="content page-22 moto-section" data-widget="section" data-container="section">
                                                    <div class="moto-widget moto-widget-row" data-widget="row" style="" data-bg-position="left top" data-draggable-disabled="">
                                                        <div class="container-fluid">
                                                            <div class="row" data-container="container">
                                                                <div class="moto-cell col-sm-12 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                                                    <div data-widget-id="wid__image__5e9dd81ee0785" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                                        <span class="moto-widget-image-link">
                                                                            <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-6.jpg" class="moto-widget-image-picture lazyloaded" data-id="179" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-6.jpg">
                                                                        </span>
                                                                    </div>
                                                                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sasa" data-visible-on="-" data-animation="" data-draggable-disabled="">
                                                                        <div id="detailPost" class="moto-widget-text-content moto-widget-text-editable moto-text_normal" aria-multiline="true">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <script>
                                                                        var content = @json($post->content);
                                                                        document.getElementById("detailPost").innerHTML = content;
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="moto-widget moto-widget-row" data-widget="row" style="" data-bg-position="left top">
                                                        <div class="container-fluid">
                                                            <div class="row" data-container="container">
                                                                <div class="moto-cell col-sm-12 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                                                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aala" data-visible-on="-" data-animation="">
                                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                                            <p class="moto-text_normal">The marketing is considered to be one of the most promising fields. This confirms the opening of new universities and specialized departments and faculties, where teachers and administration will "make" the geniuses in the field market management.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </section>

                                            </div>
                                            <!-- <div data-widget-id="wid_1587406098_2kp7kmf9o" class="moto-widget moto-widget-image moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="image">
                                                <span class="moto-widget-image-link">
                                                    <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png" class="moto-widget-image-picture lazyloaded" data-id="195" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png">
                                                </span>
                                            </div>
                                            <div data-widget-id="wid__blog_post_comments__5e9de29947bd5" class="moto-widget moto-widget-blog-post_comments moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_comments" data-preset="default">
                                                <div class="moto-widget-empty"></div>
                                                <div data-widget-id="wid__disqus__5f4b7c660b629" class="moto-widget moto-widget-disqus moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="disqus" data-params="{&quot;shortname&quot;:null,&quot;language&quot;:&quot;en&quot;,&quot;use_identifier&quot;:false,&quot;identifier&quot;:&quot;&quot;,&quot;url&quot;:&quot;@dynamic&quot;}" data-url="https://template100370.motopreview.com/blog/the-main-objectives-of-the-marketer/">
                                                    <div class="moto-widget-empty"></div>
                                                </div>
                                            </div> -->
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                    <div data-widget-id="wid_1587406098_2kp7kmf9o" class="moto-widget moto-widget-image moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="image">
                        <span class="moto-widget-image-link">
                            <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png" class="moto-widget-image-picture lazyloaded" data-id="195" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png">
                        </span>
                    </div>
                    <div data-widget-id="wid__blog_post_comments__5e9de29947bd5" class="moto-widget moto-widget-blog-post_comments moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_comments" data-preset="default">
                        <div class="moto-widget-empty"></div>
                        <div data-widget-id="wid__disqus__5f4328440d699" class="moto-widget moto-widget-disqus moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="disqus" data-params="{&quot;shortname&quot;:null,&quot;language&quot;:&quot;en&quot;,&quot;use_identifier&quot;:false,&quot;identifier&quot;:&quot;&quot;,&quot;url&quot;:&quot;@dynamic&quot;}" data-url="https://template100370.motopreview.com/blog/the-most-common-mistakes-when-managing-personal-finances/">
                            <div class="moto-widget-empty"></div>
                        </div>
                    </div>
                </div>
                <div class="moto-cell col-sm-3 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                    <div data-widget-id="wid__blog_recent_posts__5e9dd8a9b454c" class="moto-widget moto-widget-blog-recent_posts moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="blog.recent_posts">
                        <div class="moto-widget-blog-recent_posts-title">
                            <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                <div class="moto-widget-text-content moto-widget-text-editable">
                                    <p class="moto-text_system_5">Bài viết dịch vụ</p>
                                </div>
                            </div>
                        </div>

                        <div class="moto-widget moto-widget-row" data-widget="row">
                            <div class="container-fluid">
                                <div class="row moto-widget-blog-recent_posts-list">
                                    @foreach($recent_posts as $recent_post)
                                    <div class="moto-cell col-sm-12 moto-widget-blog-recent_posts-item">
                                        <div class="moto-widget-blog-recent_posts-item__content  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto">

                                            <div class="moto-widget-blog-recent_posts-item-preview">
                                                <div data-widget-id="wid__image__5f3ea9a39a795" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                    <a href="/dich-vu/{{$recent_post->slug}}" data-action="url" class="moto-widget-image-link moto-link">
                                                        <img data-src="{{$recent_post->thumbnail}}" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="{{$recent_post->thumbnail}}">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="moto-widget-blog-recent_posts-item-title">
                                                <div class="moto-widget moto-widget-text moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <h2 class="blog-post-title moto-text_system_2">
                                                            <a href="/dich-vu/{{$recent_post->slug}}">{{$recent_post->title}}</a>
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="moto-widget moto-widget-row" data-widget="row">
                                                <div class="container-fluid">
                                                    <div class="row">

                                                        <div class="moto-cell col-sm" data-container="container">
                                                            <div data-widget-id="wid__blog_post_published_on__5f3ea9a39ab7a" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                                <div class="moto-text_287">
                                                                    <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                        {{date('d-m-Y', strtotime($recent_post->created_at))}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="moto-cell col-sm" data-container="container">
                                                            <div data-widget-id="wid__blog_post_author__5f3ea9a39b0c1" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                                <div class="moto-text_287">
                                                                    <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Admin</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>