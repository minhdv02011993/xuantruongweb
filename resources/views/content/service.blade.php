<section id="section-content" class="content page-3 moto-section" data-widget="section" data-container="section">
    <div class="moto-widget moto-widget-row row-fixed moto-justify-content_center moto-bg-color5_4 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sasa" style="" data-bg-position="left top">


        <div class="container-fluid">
            <div class="row" data-container="container">


                <div class="moto-widget moto-widget-row__column moto-cell col-sm-3 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="-" data-animation="">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <h1 class="moto-text_system_9">Dịch vụ</h1>
                        </div>
                    </div>


                </div>




                <div class="moto-widget moto-widget-row__column moto-cell col-sm-9 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                    <div id="wid_1587402926_p8907vih0" class="moto-widget moto-widget-breadcrumbs moto-align-right moto-align-left_mobile-v moto-align-left_mobile-h moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto   moto-preset-default" data-widget="breadcrumbs" data-preset="default">
                        <div class="moto-widget-breadcrumbs__wrapper">
                            <ul class="moto-widget-breadcrumbs__list">
                                <li class="moto-widget-breadcrumbs__item">
                                    <a href="/" class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_link"><span class="moto-widget-breadcrumbs__item-text">Trang chủ</span></a>
                                </li>
                                <li class="moto-widget-breadcrumbs__item">
                                    <span class="moto-widget-breadcrumbs__item-delimiter">/</span>
                                    <span class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_current"><span class="moto-widget-breadcrumbs__item-text">Dịch vụ</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
    <div class="moto-widget moto-widget-row row-fixed moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-widget="row" style="" data-bg-position="left top" data-spacing="lala" data-visible-on="-">
        <div class="container-fluid">
            <div class="row" data-container="container">
                <div class="moto-cell col-sm-9 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                    <div data-widget-id="wid__blog_post_list__5e9dd8a9ab837" class="moto-widget moto-widget-blog-post_list moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="blog.post_list">

                        <ul class="moto-blog-posts-list">
                            @foreach($posts as $post)
                            <li class="moto-blog-posts-list-item">
                                <article>
                                    <div class="moto-widget moto-widget-row" data-widget="row">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="moto-cell col-sm-12" data-container="container">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div data-widget-id="wid__image__5f3ea9a39329a" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                                <a href="/dich-vu/{{$post->slug}}" data-action="blog.post" class="moto-widget-image-link moto-link">
                                                                    <img data-src="{{$post->thumbnail}}" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="{{$post->thumbnail}}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-widget="text">
                                                                <div class="moto-widget-text-content moto-widget-text-editable">
                                                                    <h2 class="moto-text_system_6">
                                                                        <a href="/dich-vu/{{$post->slug}}">{{$post->title}}</a>
                                                                    </h2>
                                                                </div>
                                                            </div>

                                                            <div class="moto-blog-post-content moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto">
                                                                <p class="moto-text_normal">{{$post->sub_content}}</p>
                                                            </div>

                                                            <div data-widget-id="wid__button__5f3ea9a39443f" class="moto-widget moto-widget-button moto-preset-2 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="button">
                                                                <a href="/dich-vu/{{$post->slug}}" data-action="blog.post" class="moto-widget-button-link moto-size-medium moto-link"><span class="fa moto-widget-theme-icon"></span> <span class="moto-widget-button-label">READ MORE</span></a>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="moto-widget moto-widget-divider moto-preset-3 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-center" data-widget="divider_horizontal" data-divider-type="horizontal" data-preset="3">
                                                        <hr class="moto-widget-divider-line">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </li>
                            @endforeach

                        </ul>

                        <!-- <div class="moto-widget moto-widget-pagination moto-preset-default moto-spacing-top-small moto-spacing-bottom-small moto-align-left" data-widget="pagination" data-preset="default">
                            <ul class="moto-pagination-group moto-pagination-group_pages">
                                <li class="moto-pagination-item moto-pagination-item-page">
                                    <span class="moto-pagination-link moto-pagination-link_active"><span class="moto-pagination-link-text">1</span></span>
                                </li>
                                <li class="moto-pagination-item moto-pagination-item-page">
                                    <a href="https://template100370.motopreview.com/blog/?page=2" class="moto-pagination-link"><span class="moto-pagination-link-text">2</span></a>
                                </li>
                            </ul>
                            <ul class="moto-pagination-group moto-pagination-group-controls moto-pagination-group_right">
                                <li class="moto-pagination-item moto-pagination-item-control moto-pagination-item-control_next">
                                    <a href="https://template100370.motopreview.com/blog/?page=2" class="moto-pagination-link"><i class="moto-pagination-link-icon moto-pagination-link-text fa fa-angle-right"></i></a>
                                </li>
                                <li class="moto-pagination-item moto-pagination-item-control moto-pagination-item-control_last">
                                    <a href="https://template100370.motopreview.com/blog/?page=2" class="moto-pagination-link"><i class="moto-pagination-link-icon moto-pagination-link-text fa fa-angle-double-right"></i></a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
                <div class="moto-cell col-sm-3 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                    <div data-widget-id="wid_1587405314_2pksyh2ma" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                        <div class="moto-widget-spacer-block" style="height:10px"></div>
                    </div>
                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" data-visible-on="-" data-animation="">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <p class="moto-text_system_5">Danh mục</p>
                        </div>
                    </div>
                    <div data-widget-id="wid_1587403798_659c898bd" class="moto-widget moto-widget-blog-category_list moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto moto-align-left  moto-widget-blog-category_list_show-delimiter moto-widget_flex-alignment" data-widget="blog.category_list" data-preset="default">
                        <div class="moto-widget-blog-category_list__content-wrapper">
                            <ul class="moto-widget-blog-category_list__items">
                                @foreach($categories as $category)
                                <li class="moto-widget-blog-category_list__item"><a href="/dich-vu/{{$category->slug}}" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">{{$category->name}}</span></a></li>
                                @endforeach
                        </div>
                    </div>
                    <div data-widget-id="wid__blog_recent_posts__5e9dd8a9b454c" class="moto-widget moto-widget-blog-recent_posts moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="blog.recent_posts">
                        <div class="moto-widget-blog-recent_posts-title">
                            <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                <div class="moto-widget-text-content moto-widget-text-editable">
                                    <p class="moto-text_system_5">Bài viết gần đây</p>
                                </div>
                            </div>
                        </div>

                        <div class="moto-widget moto-widget-row" data-widget="row">
                            <div class="container-fluid">
                                <div class="row moto-widget-blog-recent_posts-list">
                                    @foreach($recent_posts as $recent_post)
                                    <div class="moto-cell col-sm-12 moto-widget-blog-recent_posts-item">
                                        <div class="moto-widget-blog-recent_posts-item__content  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto">

                                            <div class="moto-widget-blog-recent_posts-item-preview">
                                                <div data-widget-id="wid__image__5f3ea9a39a795" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                    <a href="/dich-vu/{{$recent_post->slug}}" data-action="url" class="moto-widget-image-link moto-link">
                                                        <img data-src="{{$recent_post->thumbnail}}" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="{{$recent_post->thumbnail}}">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="moto-widget-blog-recent_posts-item-title">
                                                <div class="moto-widget moto-widget-text moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <h2 class="blog-post-title moto-text_system_2">
                                                            <a href="/dich-vu/{{$recent_post->slug}}">{{$recent_post->title}}</a>
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="moto-widget moto-widget-row" data-widget="row">
                                                <div class="container-fluid">
                                                    <div class="row">

                                                        <div class="moto-cell col-sm" data-container="container">
                                                            <div data-widget-id="wid__blog_post_published_on__5f3ea9a39ab7a" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                                <div class="moto-text_287">
                                                                    <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                        {{date('d-m-Y', strtotime($recent_post->created_at))}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="moto-cell col-sm" data-container="container">
                                                            <div data-widget-id="wid__blog_post_author__5f3ea9a39b0c1" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                                <div class="moto-text_287">
                                                                    <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Admin</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>