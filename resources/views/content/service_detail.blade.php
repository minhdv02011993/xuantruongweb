<section id="section-content" class="content page-3 moto-section" data-widget="section" data-container="section">
    <div class="moto-widget moto-widget-row row-fixed moto-justify-content_center moto-bg-color5_4 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sasa" style="" data-bg-position="left top">


        <div class="container-fluid">
            <div class="row" data-container="container">


                <div class="moto-widget moto-widget-row__column moto-cell col-sm-3 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">



                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa" data-visible-on="-" data-animation="">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <h1 class="moto-text_system_9">Dịch vụ</h1>
                        </div>
                    </div>


                </div>




                <div class="moto-widget moto-widget-row__column moto-cell col-sm-9 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">

                    <div id="wid_1587402926_p8907vih0" class="moto-widget moto-widget-breadcrumbs moto-align-right moto-align-left_mobile-v moto-align-left_mobile-h moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto   moto-preset-default" data-widget="breadcrumbs" data-preset="default">
                        <div class="moto-widget-breadcrumbs__wrapper">
                            <ul class="moto-widget-breadcrumbs__list">
                                <li class="moto-widget-breadcrumbs__item">
                                    <a href="/" class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_link"><span class="moto-widget-breadcrumbs__item-text">Trang chủ</span></a>
                                </li>
                                <li class="moto-widget-breadcrumbs__item">
                                    <span class="moto-widget-breadcrumbs__item-delimiter">/</span>
                                    <span class="moto-widget-breadcrumbs__item-content moto-widget-breadcrumbs__item-content_current"><span class="moto-widget-breadcrumbs__item-text">Dịch vụ</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>



            </div>
        </div>
    </div>
    <div class="moto-widget moto-widget-row row-fixed moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-widget="row" style="" data-bg-position="left top" data-spacing="lala" data-visible-on="-">
        <div class="container-fluid">
            <div class="row" data-container="container">

                <div class="moto-cell col-sm-9 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                    <div data-widget-id="wid_1587405465_rgplr1d3h" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                        <div class="moto-widget-spacer-block" style="height:10px"></div>
                    </div>
                    <div data-widget-id="wid__blog_post_content__5e9de29947799" class="moto-widget moto-widget-blog-post_content moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="blog.post_content">

                        <section id="section-content" class="content page-25 moto-section" data-widget="section" data-container="section">
                            <div class="moto-widget moto-widget-row" data-widget="row" style="" data-bg-position="left top" data-draggable-disabled="">
                                <div class="container-fluid">
                                    <div class="row" data-container="container">
                                        @if($post)
                                        <div class="moto-cell col-sm-9 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                            <div data-widget-id="wid__blog_post_name__5e9de29946060" class="moto-widget moto-widget-blog-post_name moto-preset-default moto-align-left moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-preset="default" data-widget="blog.post_name">
                                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <h1 class="moto-text_system_6">{{$post->title}}</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-grid-type="sm" class="moto-widget moto-widget-row row-gutter-0" data-widget="row" style="" data-bg-position="left top">
                                                <div class="container-fluid">
                                                    <div class="row" data-container="container">
                                                        <div class="moto-cell col-sm-2 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                                            <div data-widget-id="wid__blog_post_published_on__5e9de29946551" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                                <div class="moto-text_system_11">
                                                                    <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                        {{date('d-m-Y', strtotime($post->created_at))}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="moto-cell col-sm-10 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                                            <div class="moto-widget moto-widget-row moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="aaaa" style="" data-bg-position="left top">


                                                                <div class="container-fluid">
                                                                    <div class="row" data-container="container">


                                                                        <div class="moto-widget moto-widget-row__column moto-cell col-sm-3 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">
                                                                            <div data-widget-id="wid__blog_post_author__5e9de29946a6e" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                                                <div class="moto-text_system_11">
                                                                                    <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Admin</span>
                                                                                </div>
                                                                            </div>


                                                                        </div>




                                                                        <div class="moto-widget moto-widget-row__column moto-cell col-sm-9 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                                                                            <div data-widget-id="wid_1587405716_qp1zek8f1" class="moto-widget moto-widget-blog-post_category moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left " data-widget="blog.post_category">
                                                                                <div class="moto-text_system_11">
                                                                                    <a href="/tin-tuc/">
                                                                                        <span class="fa fa-folder moto-widget-blog-post_category-icon" aria-hidden="true"></span><span class="moto-widget-blog-post_category-text">Recent</span>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-widget-id="wid_1587405465_vzx3at259" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                                                <div class="moto-widget-spacer-block" style="height:10px"></div>
                                            </div>
                                            <div data-widget-id="wid__social_buttons__5e9de2994702f" class="moto-widget moto-widget-social-buttons moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="social_buttons" data-preset="default" data-spacing="aasa">

                                                <ul class="social-buttons-list">
                                                    <li class="social-button" data-name="facebook_share" data-provider="facebook">
                                                        <div class="fb-share-button fb_iframe_widget" data-href="https://template100370.motopreview.com/blog/the-main-objectives-of-the-marketer/" data-layout="button_count" moto-dependency-require="facebook" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=0&amp;href=https%3A%2F%2Ftemplate100370.motopreview.com%2Fblog%2Fthe-main-objectives-of-the-marketer%2F&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey"><span style="vertical-align: bottom; width: 77px; height: 20px;"><iframe name="f1067401740a29c" width="1000px" height="1000px" data-testid="fb:share_button Facebook Social Plugin" title="fb:share_button Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.8/plugins/share_button.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df63d9bdf960554%26domain%3Dtemplate100370.motopreview.com%26origin%3Dhttps%253A%252F%252Ftemplate100370.motopreview.com%252Ff806892e201824%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Ftemplate100370.motopreview.com%2Fblog%2Fthe-main-objectives-of-the-marketer%2F&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey" style="border: none; visibility: visible; width: 77px; height: 20px;" class=""></iframe></span></div>
                                                    </li>
                                                    <li class="social-button" data-name="twitter_tweet" data-provider="twitter"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" style="position: static; visibility: visible; width: 60px; height: 20px;" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.3c5aa8e2a38bbbee4b6d88e6846fc657.en.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=https%3A%2F%2Ftemplate100370.motopreview.com%2Fblog%2Fthe-main-objectives-of-the-marketer%2F&amp;size=m&amp;text=The%20main%20objectives%20of%20the%20marketer&amp;time=1598782658761&amp;type=share&amp;url=https%3A%2F%2Ftemplate100370.motopreview.com%2Fblog%2Fthe-main-objectives-of-the-marketer%2F"></iframe></li>
                                                    <li class="social-button" data-name="linkedIn_share" data-provider="linkedin"><span class="IN-widget" data-lnkd-debug="<script type=&quot;IN/Share+init&quot; data-url=&quot;https://template100370.motopreview.com/blog/the-main-objectives-of-the-marketer/&quot; data-counter=&quot;right&quot; moto-dependency-require=&quot;linkedin&quot;></script>" style="display: inline-block; line-height: 1; vertical-align: bottom; padding: 0px; margin: 0px; text-indent: 0px; text-align: center;"><span style="padding: 0px !important; margin: 0px !important; text-indent: 0px !important; display: inline-block !important; vertical-align: bottom !important; font-size: 1px !important;"><button class="IN-2bc0215c-7188-4274-b598-1969e06d4d7c-1G9ISYhSF8XoOmdcl0yKDu">
                                                                    <xdoor-icon aria-hidden="true"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet">
                                                                            <g style="fill: currentColor">
                                                                                <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
                                                                                <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
                                                                            </g>
                                                                        </svg></xdoor-icon>Share
                                                                </button></span></span></li>
                                                </ul>
                                            </div>
                                            <div data-widget-id="wid_1587405465_rgplr1d3h" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                                                <div class="moto-widget-spacer-block" style="height:10px"></div>
                                            </div>
                                            <div data-widget-id="wid__blog_post_content__5e9de29947799" class="moto-widget moto-widget-blog-post_content moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="blog.post_content">

                                                <section id="section-content" class="content page-22 moto-section" data-widget="section" data-container="section">
                                                    <div class="moto-widget moto-widget-row" data-widget="row" style="" data-bg-position="left top" data-draggable-disabled="">
                                                        <div class="container-fluid">
                                                            <div class="row" data-container="container">
                                                                <div class="moto-cell col-sm-12 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                                                    <div data-widget-id="wid__image__5e9dd81ee0785" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                                        <span class="moto-widget-image-link">
                                                                            <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-6.jpg" class="moto-widget-image-picture lazyloaded" data-id="179" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/mt-2006-blog-img-6.jpg">
                                                                        </span>
                                                                    </div>
                                                                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sasa" data-visible-on="-" data-animation="" data-draggable-disabled="">
                                                                        <div id="detailPost" class="moto-widget-text-content moto-widget-text-editable moto-text_normal" aria-multiline="true">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <script>
                                                                        var content = @json($post->content);
                                                                        document.getElementById("detailPost").innerHTML = content;
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="moto-widget moto-widget-row" data-widget="row" style="" data-bg-position="left top">
                                                        <div class="container-fluid">
                                                            <div class="row" data-container="container">
                                                                <div class="moto-cell col-sm-12 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                                                                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aala" data-visible-on="-" data-animation="">
                                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                                            <p class="moto-text_normal">The marketing is considered to be one of the most promising fields. This confirms the opening of new universities and specialized departments and faculties, where teachers and administration will "make" the geniuses in the field market management.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </section>

                                            </div>
                                            <!-- <div data-widget-id="wid_1587406098_2kp7kmf9o" class="moto-widget moto-widget-image moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="image">
                                                <span class="moto-widget-image-link">
                                                    <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png" class="moto-widget-image-picture lazyloaded" data-id="195" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png">
                                                </span>
                                            </div>
                                            <div data-widget-id="wid__blog_post_comments__5e9de29947bd5" class="moto-widget moto-widget-blog-post_comments moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_comments" data-preset="default">
                                                <div class="moto-widget-empty"></div>
                                                <div data-widget-id="wid__disqus__5f4b7c660b629" class="moto-widget moto-widget-disqus moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="disqus" data-params="{&quot;shortname&quot;:null,&quot;language&quot;:&quot;en&quot;,&quot;use_identifier&quot;:false,&quot;identifier&quot;:&quot;&quot;,&quot;url&quot;:&quot;@dynamic&quot;}" data-url="https://template100370.motopreview.com/blog/the-main-objectives-of-the-marketer/">
                                                    <div class="moto-widget-empty"></div>
                                                </div>
                                            </div> -->
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                    <div data-widget-id="wid_1587406098_2kp7kmf9o" class="moto-widget moto-widget-image moto-preset-default  moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="image">
                        <span class="moto-widget-image-link">
                            <img data-src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png" class="moto-widget-image-picture lazyloaded" data-id="195" title="" alt="" src="/mt-demo/100300/100370/mt-content/uploads/2020/04/disqus.png">
                        </span>
                    </div>
                    <div data-widget-id="wid__blog_post_comments__5e9de29947bd5" class="moto-widget moto-widget-blog-post_comments moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_comments" data-preset="default">
                        <div class="moto-widget-empty"></div>
                        <div data-widget-id="wid__disqus__5f4328440d699" class="moto-widget moto-widget-disqus moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="disqus" data-params="{&quot;shortname&quot;:null,&quot;language&quot;:&quot;en&quot;,&quot;use_identifier&quot;:false,&quot;identifier&quot;:&quot;&quot;,&quot;url&quot;:&quot;@dynamic&quot;}" data-url="https://template100370.motopreview.com/blog/the-most-common-mistakes-when-managing-personal-finances/">
                            <div class="moto-widget-empty"></div>
                        </div>
                    </div>
                </div>
                <div class="moto-cell col-sm-3 moto-widget moto-widget-row__column" data-container="container" data-widget="row.column" style="" data-bg-position="left top">
                    <div data-widget-id="wid_1587405314_2pksyh2ma" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="aaaa" data-visible-on="+desktop,tablet,mobile-h,mobile-v">
                        <div class="moto-widget-spacer-block" style="height:10px"></div>
                    </div>
                    <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" data-visible-on="-" data-animation="">
                        <div class="moto-widget-text-content moto-widget-text-editable">
                            <p class="moto-text_system_5">Danh mục</p>
                        </div>
                    </div>
                    <div data-widget-id="wid_1587403798_659c898bd" class="moto-widget moto-widget-blog-category_list moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto moto-align-left  moto-widget-blog-category_list_show-delimiter moto-widget_flex-alignment" data-widget="blog.category_list" data-preset="default">
                        <div class="moto-widget-blog-category_list__content-wrapper">
                            <ul class="moto-widget-blog-category_list__items">
                                @foreach($categories as $category)
                                <li class="moto-widget-blog-category_list__item"><a href="/dich-vu/{{$category->slug}}" class="moto-widget-blog-category_list__item-link moto-widget__flex-alignment-element"><span class="moto-widget-blog-category_list__item-icon fa fa-check-circle" aria-hidden="true"></span><span class="moto-widget-blog-category_list__item-text">{{$category->name}}</span></a></li>
                                @endforeach
                        </div>
                    </div>
                    <div data-widget-id="wid__blog_recent_posts__5e9dd8a9b454c" class="moto-widget moto-widget-blog-recent_posts moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="blog.recent_posts">
                        <div class="moto-widget-blog-recent_posts-title">
                            <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                <div class="moto-widget-text-content moto-widget-text-editable">
                                    <p class="moto-text_system_5">Bài viết gần đây</p>
                                </div>
                            </div>
                        </div>

                        <div class="moto-widget moto-widget-row" data-widget="row">
                            <div class="container-fluid">
                                <div class="row moto-widget-blog-recent_posts-list">
                                    @foreach($recent_posts as $recent_post)
                                    <div class="moto-cell col-sm-12 moto-widget-blog-recent_posts-item">
                                        <div class="moto-widget-blog-recent_posts-item__content  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto">

                                            <div class="moto-widget-blog-recent_posts-item-preview">
                                                <div data-widget-id="wid__image__5f3ea9a39a795" class="moto-widget moto-widget-image moto-preset-4 moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="image">
                                                    <a href="/dich-vu/{{$recent_post->slug}}" data-action="url" class="moto-widget-image-link moto-link">
                                                        <img data-src="{{$recent_post->thumbnail}}" class="moto-widget-image-picture lazyloaded" data-id="" title="" alt="" src="{{$recent_post->thumbnail}}">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="moto-widget-blog-recent_posts-item-title">
                                                <div class="moto-widget moto-widget-text moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto moto-align-left" data-preset="default" data-spacing="aasa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <h2 class="blog-post-title moto-text_system_2">
                                                            <a href="/dich-vu/{{$recent_post->slug}}">{{$recent_post->title}}</a>
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="moto-widget moto-widget-row" data-widget="row">
                                                <div class="container-fluid">
                                                    <div class="row">

                                                        <div class="moto-cell col-sm" data-container="container">
                                                            <div data-widget-id="wid__blog_post_published_on__5f3ea9a39ab7a" class="moto-widget moto-widget-blog-post_published_on moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="default" data-widget="blog.post_published_on" data-spacing="aasa">
                                                                <div class="moto-text_287">
                                                                    <span class="fa fa-calendar moto-widget-blog-post_published_on-icon"></span><span class="moto-widget-blog-post_published_on-date">
                                                                        {{date('d-m-Y', strtotime($recent_post->created_at))}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="moto-cell col-sm" data-container="container">
                                                            <div data-widget-id="wid__blog_post_author__5f3ea9a39b0c1" class="moto-widget moto-widget-blog-post-author moto-preset-default moto-align-left moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="blog.post_author" data-preset="default">
                                                                <div class="moto-text_287">
                                                                    <span class="fa fa-user moto-widget-blog-post-author-icon"></span><span class="moto-widget-blog-post-author-text">Admin</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>