<!DOCTYPE html>
<!-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> -->
<html lang="en" data-ng-app="website">
@include('partials.head')

<body class="moto-background moto-website_live moto-browser_chrome">

    <div class="page">
        @include('partials.header')
        @yield('content')
    </div>
    @include('partials.footer')
</body>

</html>