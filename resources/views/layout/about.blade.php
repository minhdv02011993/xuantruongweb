@extends('layout')

@section('title', "About")

@section('content')

@include('content.about')

@endsection