@extends('layout')

@section('title', $post->title)

@section('content')

@include('content.experience_detail')

@endsection