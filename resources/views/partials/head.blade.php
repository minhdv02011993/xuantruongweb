<head>
    <style type="text/css">
        @charset "UTF-8";

        [ng\:cloak],
        [ng-cloak],
        [data-ng-cloak],
        [x-ng-cloak],
        .ng-cloak,
        .x-ng-cloak,
        .ng-hide:not(.ng-hide-animate) {
            display: none !important;
        }

        ng\:form {
            display: block;
        }

        .ng-animate-shim {
            visibility: hidden;
        }

        .ng-anchor {
            position: absolute;
        }
    </style>


    <meta charset="utf-8">
    <title>@yield('title')</title>
    <!-- <title>Home</title> -->
    <link rel="SHORTCUT ICON" href="/img/favicon.jpg" type="image/vnd.microsoft.icon">


    <link rel="canonical" href="https://template100370.motopreview.com/">
    <meta property="og:title" content="Home">
    <meta property="og:url" content="https://template100370.motopreview.com/">
    <meta property="og:type" content="website">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link href="https://s3sf.tmimgcdn.com/plasma-platform.spa.chunk.1b12daaeb147a12ce2ec.css" rel="stylesheet">
    <link href="https://s3sf.tmimgcdn.com/index.spa.chunk.d2aaa4dd486d5f54be39.css" rel="stylesheet">
    <link href="https://s3sf.tmimgcdn.com/page_livedemo.spa.chunk.f9cbf698204cb1bff968.css" rel="stylesheet">
    <link rel="stylesheet" href="https://template100370.motopreview.com/mt-includes/css/assets.min.css?_build=1595932327">
    <link rel="stylesheet" href="{{ url('/css/assets.min.css') }}">
    <style>
        @import url(//fonts.googleapis.com/css?family=Poppins:200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic&subset=latin);
    </style>
    <link rel="stylesheet" href="{{ url('/css/styles.css') }}">
    
</head>