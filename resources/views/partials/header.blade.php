<header id="section-header" class="header moto-section" data-widget="section" data-container="section">
    
    <div class="moto-widget moto-widget-container undefined moto-container_header_5e9b50e71 moto-sticky__bootstrapped" data-visible-on="" data-widget="container" data-container="container" data-css-name="moto-container_header_5e9b50e71" data-bg-position="left top" data-moto-sticky="{}" style="">



        <div class="moto-widget moto-widget-row row-fixed moto-justify-content_center moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-grid-type="sm" data-widget="row" data-visible-on="-" data-spacing="sasa" style="" data-bg-position="left top">


            <div class="container-fluid">
                <div class="row" data-container="container">


                    <div class="moto-widget moto-widget-row__column moto-cell col-sm-3 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                        <div data-widget-id="wid_1587237109_semv10a0p" class="moto-widget moto-widget-image moto-preset-default moto-align-center_mobile-h moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="image">
                            <a href="/" data-action="page" class="moto-widget-image-link moto-link">
                                <img data-src="/img/logoxuantruong.png" class="moto-widget-image-picture lazyloaded" data-id="145" title="" alt="" src="/img/logoxuantruong.png">
                            </a>
                        </div>
                    </div>
                    <div class="moto-widget moto-widget-row__column moto-cell col-sm-8 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                        <div data-widget-id="wid_1587237516_57au3q9c1" class="moto-widget moto-widget-menu moto-preset-default moto-align-right moto-align-center_mobile-h moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-preset="default" data-widget="menu">
                            <a href="#" class="moto-widget-menu-toggle-btn"><i class="moto-widget-menu-toggle-btn-icon fa fa-bars"></i></a>
                            <ul class="moto-widget-menu-list moto-widget-menu-list_horizontal">
                                <li class="moto-widget-menu-item">
                                    <a href="/" data-action="page" id="homeLink" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">Trang chủ</a>
                                </li>
                                <li class="moto-widget-menu-item">
                                    <a href="/gioi-thieu/" id="introduceLink" data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">Giới thiệu</a>
                                </li>
                                <li class="moto-widget-menu-item">
                                    <a href="/kinh-nghiem/" id="experienceLink" data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">Kinh nghiệm</a>
                                </li>
                                <li class="moto-widget-menu-item">
                                    <a href="/tin-tuc/" id="newLink" data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">Tin tức</a>
                                </li>
                                <li class="moto-widget-menu-item">
                                    <a href="/tuyen-dung/" id="recruitLink" data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">Tuyển dụng</a>
                                </li>
                                <li class="moto-widget-menu-item">
                                    <a href="/lien-he/" id="contactLink" data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div class="moto-widget moto-widget-row__column moto-cell col-sm-1 moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" style="" data-widget="row.column" data-container="container" data-spacing="aaaa" data-bg-position="left top">


                        <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sasa" data-visible-on="-" data-animation="">
                            <div class="moto-widget-text-content moto-widget-text-editable">
                                <p class="moto-text_system_10" style="text-align: center;"><a  href="javascript:void(0)"><span class="fa"></span></a></p>
                            </div>
                        </div>
                    </div> -->

                    <script>
                        $(document).ready(function() {
                            
                            var pathName = location.pathname;
                            if (pathName == '/') $('#homeLink').addClass('moto-widget-menu-link-active');
                            if (pathName.includes('/gioi-thieu')) $('#introduceLink').addClass('moto-widget-menu-link-active');
                            if (pathName.includes('/kinh-nghiem')) $('#experienceLink').addClass('moto-widget-menu-link-active');
                            if (pathName.includes('/tin-tuc')) $('#newLink').addClass('moto-widget-menu-link-active');
                            if (pathName.includes('/tuyen-dung')) $('#recruitLink').addClass('moto-widget-menu-link-active');
                            if (pathName.includes('/lien-he')) $('#contactLink').addClass('moto-widget-menu-link-active');
                        });
                        
                        console.log(location)
                    </script>

                </div>
            </div>
        </div>
    </div>
    <div class="moto-sticky-pseudo-element" style="height: 88px; width: 1502px; display: none;"></div>
</header>