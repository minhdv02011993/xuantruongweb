<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.home');
});

Route::get('/lien-he', function () {
    return view('layout.contact');
});



Route::namespace('webs')->group(function () {
    Route::get('/kinh-nghiem', 'ExperienceController@index');
    Route::get('/kinh-nghiem/{post_slug}', 'ExperienceController@detail');
    Route::get('/tin-tuc', 'NewController@index');
    Route::get('/tin-tuc/{post_slug}', 'NewController@detail');
    Route::get('/tin-tuc/{category_slug}/{post_slug}', 'newController@detail2');
    Route::get('/dich-vu', 'ServiceController@index');
    Route::get('/dich-vu/{post_slug}', 'ServiceController@detail');

    Route::get('/gioi-thieu', 'IntroduceController@index');
    
    Route::get('/tuyen-dung', 'RecruitController@index');

    Route::post('message', 'MessageController@store')->name('message');
});
